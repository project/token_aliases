# Token Aliases

This module allows it to use Token replacements with aliases, when one type is used with more than one entity.
You have to use the $data argument to make it work.

```php
  $token_service = \Drupal::service('token_aliases');
  $string = 'Hello [user1:username], we have news from [user2:username] see [node:title] and [my_node:title] for more info.';
  // Fully loaded Drupal entities which are keys with the aliases used in the string.
  $data = [
    'user1' => $user1,
    'user2' => $user2,
    'node' => $node,
    'my_node' => $my_node,
  ];
  $result = $token_service->plainReplace($string, $data);
```
