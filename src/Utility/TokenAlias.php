<?php

namespace Drupal\token_aliases\Utility;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;

/**
 * Replaces all tokens in given markup with appropriate values and supports alias for Drupal entities.
 *
 * Warning: This Token extension uses a simple multi-pass strategy to achieve its goal.
 *
 * @param string $markup
 *   An HTML string containing replaceable tokens.
 * @param array $data
 *   (optional) An array of keyed objects. For simple replacement scenarios
 *   'node', 'user', and others are common keys, with an accompanying node or
 *   user object being the value. Some token types, like 'site', do not require
 *   any explicit information from $data and can be replaced even if it is
 *   empty.
 *   To use an entity type more than once via alias it is possible to use an
 *   arbitrary alias as key, which need to point to an entity which implements
 *   the EntityInterface. This will be used to obtain the original type of the
 *   object.
 *   @code
 *     $string = 'Hello [user1:username], we have news from [user2:username]';
 *     $data = ['user1' => $user1, 'user2' => $user2]; // Fully loaded User entities.
 *     $result = $token_service->plainReplace($string, $data);
 *   @endcode
 * @param array $options
 *   (optional) A keyed array of settings and flags to control the token
 *   replacement process. Supported options are:
 *   - langcode: A language code to be used when generating locale-sensitive
 *     tokens.
 *   - callback: A callback function that will be used to post-process the
 *     array of token replacements after they are generated.
 *   - clear: A boolean flag indicating that tokens should be removed from the
 *     final text if no replacement value can be generated.
 * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
 *   (optional) An object to which static::generate() and the hooks and
 *   functions that it invokes will add their required bubbleable metadata.
 *
 *   To ensure that the metadata associated with the token replacements gets
 *   attached to the same render array that contains the token-replaced text,
 *   callers of this method are encouraged to pass in a BubbleableMetadata
 *   object and apply it to the corresponding render array. For example:
 *   @code
 *     $bubbleable_metadata = new BubbleableMetadata();
 *     $build['#markup'] = $token_service->replace('Tokens: [node:nid] [current-user:uid]', ['node' => $node], [], $bubbleable_metadata);
 *     $bubbleable_metadata->applyTo($build);
 *   @endcode
 *
 *   When the caller does not pass in a BubbleableMetadata object, this
 *   method creates a local one, and applies the collected metadata to the
 *   Renderer's currently active render context.
 *
 * @return string
 *   The token result is the entered HTML text with tokens replaced. The
 *   caller is responsible for choosing the right sanitization, for example
 *   the result can be put into #markup, in which case it would be sanitized
 *   by Xss::filterAdmin().
 *
 *   The return value must be treated as unsafe even if the input was safe
 *   markup. This is necessary because an attacker could craft an input
 *   string and token value that, although each safe individually, would be
 *   unsafe when combined by token replacement.
 *
 * @see static::replacePlain()
 * @see Token::doReplace()
 */
class TokenAlias extends Token {

  /**
   * {@inheritDoc}
   */
  protected function doReplace(bool $markup, string $text, array $data, array $options, BubbleableMetadata $bubbleable_metadata = NULL): string {
    $aliases = $this->aliasScan($data);

    // We remove the aliases from the data.
    $objects = [];
    foreach ($aliases as $alias => $type) {
      $objects[$alias] = $data[$alias];
      unset($data[$alias]);
    }

    // Here we store if $options['clear'] was true.
    $clear = FALSE;
    if (isset($options['clear']) && $options['clear'] === TRUE) {
      $clear = TRUE;

      // Because we have multiple passes, we set it to false.
      $options['clear'] = FALSE;
    }

    // Here we should run the normal replace, to get rid of possible duplicate arguments.
    $text = parent::doReplace($markup, $text, $data, $options, $bubbleable_metadata);

    while (TRUE) {
      if (!count($aliases)) {
        break;
      }

      $data = [];
      $typesInUse = [];
      $replacers = [];
      foreach ($aliases as $alias => $type) {
        // Here we modify the text and search and replace the aliases from [my_alias:title] to [node:title].
        if (in_array($type, $typesInUse)) {
          continue;
        }

        $replacers['[' . $alias . ':'] = '[' . $type . ':';
        $typesInUse[] = $type;
        $data[$type] = $objects[$alias];

        unset($aliases[$alias]);
      }

      $search = array_keys($replacers);
      $replace = array_values($replacers);

      $text = str_replace($search, $replace, $text);

      // We call the normal Token method
      $text = parent::doReplace($markup, $text, $data, $options, $bubbleable_metadata);
    }

    // If the 'clear' option was set, we have another run, which should clear unused tokens.
    if ($clear) {
      $data = [];
      $options['clear'] = TRUE;
      $text = parent::doReplace($markup, $text, $data, $options, $bubbleable_metadata);
    }

    return $text;
  }

  /**
   * Scans the data for aliases.
   *
   * Every time a mismatch between the key and the EntityTypeId is found
   * it counts as a match.
   */
  protected function aliasScan(array $data): array {
    $aliases = [];
    foreach ($data as $alias => $entity) {
      if (!is_object($entity)) {
        continue;
      }

      if (!$entity instanceof EntityInterface) {
        continue;
      }

      if ($entity->getEntityTypeId() === $alias) {
        continue;
      }

      $aliases[$alias] = $entity->getEntityTypeId();
    }

    return $aliases;
  }

}
